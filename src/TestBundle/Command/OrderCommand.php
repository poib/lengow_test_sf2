<?php
namespace TestBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TestBundle\Entity\L_Order;

class OrderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('test:fetch-orders')

            // the short description shown while running "php bin/console list"
            ->setDescription('Fetch orders and insert them in database')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Fetch orders and insert them in database')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fetchedOrders = $this->getContainer()->get('lengow_test')->getOrders();
        $em = $this->getContainer()->get('doctrine')->getManager();

        foreach($fetchedOrders as $fetchedOrder){
            $output->writeln('New order from '.$fetchedOrder['marketplace'].' : '.$fetchedOrder['id']);

            $order = new L_Order();
            $order->setOrderId($fetchedOrder['id']);
            $order->setMarketplace($fetchedOrder['marketplace']);
            $order->setAmount($fetchedOrder['amount']);
            $order->setCurrency($fetchedOrder['currency']);

            $em->persist($order);
        }

        $em->flush();
        $output->writeln('FINISHED.');
    }
}