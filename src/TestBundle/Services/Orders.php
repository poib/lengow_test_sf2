<?php

namespace TestBundle\Services;

use Psr\Log\LoggerInterface;
use Goutte\Client;

class Orders
{
    private $url;

    private $logger;

    private $crawler;

    /**
     * HTTPClient constructor.
     *
     * @param \Monolog\Logger $logger
     * @param $url
     */
    public function __construct(LoggerInterface $logger, $url)
    {
        $this->logger = $logger;
        $this->url = $url;
    }


    private function getCrawler(){
        if($this->crawler === null){
            $this->logger->info('New request : '.$this->url);

            $client = new Client();
            $this->crawler = $client->request('GET', $this->url);

            $this->logger->info('Request completed.');
        }
        return $this->crawler;
    }


    /**
     *
     */
    public function getOrders(){
        $orders = $this->getCrawler()->filter('orders order')->each(function ($node){
            return array(
                'id' => $node->filter('order_id')->text(),
                'marketplace' => $node->filter('marketplace')->text(),
                'amount' => $node->filter('order_amount')->text(),
                'currency' => $node->filter('order_currency')->text(),
            );
        });

        return $orders;
    }
}