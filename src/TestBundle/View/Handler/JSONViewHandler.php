<?php

namespace TestBundle\View\Handler;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JSONViewHandler
{
    /**
     * @param ViewHandler $viewHandler
     * @param View        $view
     * @param Request     $request
     * @param string      $format
     *
     * @return Response
     */
    public function createResponse(ViewHandler $handler, View $view, Request $request, $format)
    {
        $data['code'] = $view->getResponse()->getStatusCode();
        $data['success'] = $view->getResponse()->isSuccessful();
        $data['data'] = $view->getData();

        $view->setData($data);

        return $handler->createResponse($view, $request, $format);
    }
}