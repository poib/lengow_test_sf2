<?php

namespace TestBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use TestBundle\DependencyInjection\TestBundleExtension;

class TestBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new TestBundleExtension();
    }
}
