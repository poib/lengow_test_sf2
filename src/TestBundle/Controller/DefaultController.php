<?php

namespace TestBundle\Controller;

use APY\DataGridBundle\Grid\Source\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TestBundle\Entity\L_Order;
use TestBundle\Form\L_OrderType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $source = new Entity('TestBundle:L_Order');
        $grid = $this->get('grid');
        $grid->setSource($source);
        return $grid->getGridResponse('TestBundle:Default:index.html.twig');
    }

    public function createOrderAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $order = new L_Order();
        $form = $this->createForm(new L_OrderType(), $order);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($order);
            $em->flush();

            return $this->redirectToRoute('test_homepage');
        }

        return $this->render('TestBundle:Default:createOrder.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
