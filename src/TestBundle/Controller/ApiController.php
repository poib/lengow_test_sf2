<?php

namespace TestBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends FOSRestController
{
    public function getOrdersAction()
    {
        $orders = $this->getDoctrine()
            ->getRepository("TestBundle:L_Order")
            ->findAll();

        $view = $this->view($orders);
        return $this->handleView($view);
    }

    public function getOrderAction($id)
    {
        $order = $this->getDoctrine()
            ->getRepository("TestBundle:L_Order")
            ->findOneById($id);

        $view = $this->view($order);

        if(!$order){
            $view = $this->view(null, 404);
        }

        return $this->handleView($view);
    }
}
