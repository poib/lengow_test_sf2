<?php

namespace TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class L_OrderType extends AbstractType
{

    private $currencies = array(
        'EUR' => 'Euro (EUR)',
        'USD' => 'Dollar américain (USD)'
    );

    private $marketplaces = array(
        'cdiscount' => 'CDiscount',
        'amazon' => 'Amazon',
        'fnac.com' => 'Fnac.com',
        'ebay' => 'eBay',
        'priceminister' => 'PriceMinister'
    );

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('orderId')
            ->add('marketplace', 'choice', array(
                'choices' => $this->marketplaces,
                )
            )
            ->add('amount', 'money', array(
                'scale' => 2,
                'currency' => false
                )
            )
            ->add('currency', 'choice', array(
                'choices' => $this->currencies
                )
            )
            ->add('Ajouter', 'submit')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TestBundle\Entity\L_Order'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'testbundle_l_order';
    }
}
