# Test technique Symfony 2

## Installation du projet

L'installation se fait via [Composer](https://getcomposer.org) avec la commande
``composer install``

## Configuration de la base de données

Pour créer la structure de la base de données, configurez les données de connexion à la base de données situé dans ``app/config/parameters.yml``

```
# app/config/parameters.yml
parameters:
    database_host: 127.0.0.1
    database_port: null
    database_name: lengow_test_sf2
    database_user: root
    database_password: null
    mailer_transport: smtp
    mailer_host: 127.0.0.1
    mailer_user: null
    mailer_password: null
    secret: ThisTokenIsNotSoSecretChangeIt
```

Exécutez ensuite la commande ``php app/console doctrine:schema:update --force``
Vous pouvez vérifier les requêtes SQL en éxécutant ``php app/console doctrine:schema:update --dump-sql``

## Enregistrement des commandes dans la base de données

L'enregistrement des données se fait par commande. Exécutez la commande ``php app/console test:fetch-orders`` qui se chargera de récupérer et d'enregistrer les commandes dans la base de données.

## Routes crées

Vous les retrouverez dans ``src/TestBundle/Resources/config/routing.yml``

```
Page d'accueil                /
Enregistrer une commande      /create-order
Afficher toute les commandes  /api.{_format}
Afficher une commande par id  /api/{id}.{_format}

{id} correspond à l'id de la commande
{_format} correspondant au format de récupération souhaité : json, xml, yml

En laissant le paramètre {_format} vide, Symfony retournera les commandes au format json par défaut.
```